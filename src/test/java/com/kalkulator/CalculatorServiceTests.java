package com.kalkulator;

import com.kalkulator.Calculator.CalculationDTO;
import com.kalkulator.Calculator.CalculatorService;
import com.kalkulator.Calculator.CalculatorServiceImpl;
import com.kalkulator.Calculator.SummaryDTO;
import com.kalkulator.Country.NbpApi;
import com.kalkulator.Exception.CountryNotFoundException;
import org.junit.Assert;
import org.junit.Test;


public class CalculatorServiceTests {
    CalculatorService calculatorService = new CalculatorServiceImpl();

    @Test
    public void choosePolandTest() throws Exception {
        SummaryDTO serviceSummary = calculatorService.calculateEarnings(new CalculationDTO("PL", 200));

        Assert.assertEquals(serviceSummary.getCurrency(),"PLN");
        Assert.assertEquals(serviceSummary.getSalary(), 4400, 0.01);
        Assert.assertEquals(serviceSummary.getFixedCosts(), 1200, 0.01);
        Assert.assertEquals(serviceSummary.getTaxValue(),836,0.01 );
        Assert.assertEquals(serviceSummary.getEarnings(), 2364, 0.01);
    }

    @Test
    public void chooseGermanyTest() throws Exception {
        SummaryDTO serviceSummary = calculatorService.calculateEarnings(new CalculationDTO("DE", 200));
        double course = NbpApi.get("EUR");

        Assert.assertEquals(serviceSummary.getCurrency(),"EUR");
        Assert.assertEquals(serviceSummary.getSalary(), 4400, 0.01);
        Assert.assertEquals(serviceSummary.getFixedCosts(), 800, 0.01);
        Assert.assertEquals(serviceSummary.getTaxValue(),880,0.01 );
        Assert.assertEquals(serviceSummary.getEarnings(), 2720* course, 0.01);
    }

    @Test
    public void chooseUkTest() throws Exception {
        SummaryDTO serviceSummary = calculatorService.calculateEarnings(new CalculationDTO("UK", 200));
        double course = NbpApi.get("GBP");

        Assert.assertEquals(serviceSummary.getCurrency(),"GBP");
        Assert.assertEquals(serviceSummary.getSalary(), 4400, 0.01);
        Assert.assertEquals(serviceSummary.getFixedCosts(), 600, 0.01);
        Assert.assertEquals(serviceSummary.getTaxValue(),1100,0.01 );
        Assert.assertEquals(serviceSummary.getEarnings(), 2700* course, 0.01);
    }

    @Test(expected = CountryNotFoundException.class)
    public void chooseUnknownCountryTest() throws Exception {
        calculatorService.calculateEarnings(new CalculationDTO("XYZ", 200));
    }
}