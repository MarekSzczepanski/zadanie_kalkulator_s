package com.kalkulator;

import com.kalkulator.Calculator.SummaryDTO;
import com.kalkulator.Country.Germany;
import com.kalkulator.Country.NbpApi;
import com.kalkulator.Country.Poland;
import com.kalkulator.Country.UnitedKingdom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountriesTests {

	@Test
	public void calculateEarningsInPolandTest(){
		SummaryDTO summaryDTO = Poland.getInstance().calculateEarnings(200);

		Assert.assertEquals(summaryDTO.getCurrency(),"PLN");
		Assert.assertEquals(summaryDTO.getSalary(), 4400, 0.01);
		Assert.assertEquals(summaryDTO.getFixedCosts(), 1200, 0.01);
		Assert.assertEquals(summaryDTO.getTaxValue(),836,0.01 );
		Assert.assertEquals(summaryDTO.getEarnings(), 2364, 0.01);
	}

	@Test
	public void calculateEarningsInGermanyTest() throws Exception {
		SummaryDTO summaryDTO = Germany.getInstance().calculateEarnings(200);
		double course = NbpApi.get("EUR");

		Assert.assertEquals(summaryDTO.getCurrency(),"EUR");
		Assert.assertEquals(summaryDTO.getSalary(), 4400, 0.01);
		Assert.assertEquals(summaryDTO.getFixedCosts(), 800, 0.01);
		Assert.assertEquals(summaryDTO.getTaxValue(),880,0.01 );
		Assert.assertEquals(summaryDTO.getEarnings(), 2720* course, 0.01);
	}

	@Test
	public void calculateEarningsInUkTest() throws Exception {
		SummaryDTO summaryDTO = UnitedKingdom.getInstance().calculateEarnings(200);
		double course = NbpApi.get("GBP");

		Assert.assertEquals(summaryDTO.getCurrency(),"GBP");
		Assert.assertEquals(summaryDTO.getSalary(), 4400, 0.01);
		Assert.assertEquals(summaryDTO.getFixedCosts(), 600, 0.01);
		Assert.assertEquals(summaryDTO.getTaxValue(),1100,0.01 );
		Assert.assertEquals(summaryDTO.getEarnings(), 2700* course, 0.01);
	}

}