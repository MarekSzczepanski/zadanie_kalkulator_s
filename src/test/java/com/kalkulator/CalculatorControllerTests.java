package com.kalkulator;

import com.kalkulator.Calculator.*;
import com.kalkulator.Country.NbpApi;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CalculatorControllerTests {
    CalculatorService calculatorService = new CalculatorServiceImpl();
    CalculatorController calculatorController = new CalculatorController(calculatorService);

    @Test
    public void calculateEarningsInPolandTest() throws Exception {
        ResponseEntity<?> response = calculatorController.calculateEarnings(new CalculationDTO("PL", 200));
        SummaryDTO responseBody = (SummaryDTO) response.getBody();

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(responseBody.getCurrency(),"PLN");
        Assert.assertEquals(responseBody.getSalary(), 4400, 0.01);
        Assert.assertEquals(responseBody.getFixedCosts(), 1200, 0.01);
        Assert.assertEquals(responseBody.getTaxValue(),836,0.01 );
        Assert.assertEquals(responseBody.getEarnings(), 2364, 0.01);
    }

    @Test
    public void calculateEarningsInGermanyTest() throws Exception {
        ResponseEntity<?> response = calculatorController.calculateEarnings(new CalculationDTO("DE", 200));
        SummaryDTO responseBody = (SummaryDTO) response.getBody();
        double course = NbpApi.get("EUR");

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(responseBody.getCurrency(),"EUR");
        Assert.assertEquals(responseBody.getSalary(), 4400, 0.01);
        Assert.assertEquals(responseBody.getFixedCosts(), 800, 0.01);
        Assert.assertEquals(responseBody.getTaxValue(),880,0.01 );
        Assert.assertEquals(responseBody.getEarnings(), 2720* course, 0.01);
    }

    @Test
    public void calculateEarningsInUnitedKingdomTest() throws Exception {
        ResponseEntity<?> response = calculatorController.calculateEarnings(new CalculationDTO("UK", 200));
        SummaryDTO responseBody = (SummaryDTO) response.getBody();
        double course = NbpApi.get("GBP");

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(responseBody.getCurrency(),"GBP");
        Assert.assertEquals(responseBody.getSalary(), 4400, 0.01);
        Assert.assertEquals(responseBody.getFixedCosts(), 600, 0.01);
        Assert.assertEquals(responseBody.getTaxValue(),1100,0.01 );
        Assert.assertEquals(responseBody.getEarnings(), 2700* course, 0.01);
    }

    @Test
    public void chooseUnknownCountryTest() throws Exception {
        ResponseEntity<?> response = calculatorController.calculateEarnings(new CalculationDTO("XYZ", 200));
        String message = (String) response.getBody();

        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals(message, "Nie znaleziono takiego kraju");

    }
}