package com.kalkulator.Country;

import com.kalkulator.Constans.Currency;
import com.kalkulator.Constans.FixedCosts;
import com.kalkulator.Constans.Tax;

public class Poland extends Country {
    private static Poland instance = null;

    private Poland() {
        super(1,Tax.POLAND, FixedCosts.POLAND, Currency.POLAND);
    }

    public static Poland getInstance() {
        if(instance == null) {
            instance = new Poland();
        }else{
            course = 1d;
        }
        return instance;
    }
}