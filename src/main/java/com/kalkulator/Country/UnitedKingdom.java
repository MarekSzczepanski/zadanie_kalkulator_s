package com.kalkulator.Country;

import com.kalkulator.Constans.Currency;
import com.kalkulator.Constans.FixedCosts;
import com.kalkulator.Constans.Tax;

public class UnitedKingdom extends Country {
    private static UnitedKingdom instance = null;

    public UnitedKingdom() throws Exception {
        super(NbpApi.get("GBP"),Tax.UK, FixedCosts.UK, Currency.UK);
    }

    public static UnitedKingdom getInstance() throws Exception {
        if(instance == null) {
            instance = new UnitedKingdom();
        }else{
            course = NbpApi.get("GBP");
        }
        return instance;
    }
}