package com.kalkulator.Country;

import com.kalkulator.Constans.Currency;
import com.kalkulator.Constans.FixedCosts;
import com.kalkulator.Constans.Tax;

public class Germany extends Country {
    private static Germany instance = null;

    public Germany() throws Exception {
        super( NbpApi.get("EUR"), Tax.GERMANY, FixedCosts.GERMANY, Currency.GERMANY);
    }

    public static Germany getInstance() throws Exception {
        if(instance == null) {
            instance = new Germany();
        }else{
            course = NbpApi.get("EUR");
        }
        return instance;
    }
}