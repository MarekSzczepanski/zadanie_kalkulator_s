package com.kalkulator.Country;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NbpApi {

    public static double get(String country) throws Exception {
        URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/A/" + country);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        JSONObject jsonObj = new JSONObject(content.toString());
        jsonObj = new JSONObject(jsonObj.getJSONArray("rates").get(0).toString());
        return jsonObj.getDouble("mid");
    }
}