package com.kalkulator.Country;

import com.kalkulator.Calculator.SummaryDTO;
import org.apache.commons.math.util.MathUtils;

public abstract class Country {
    protected static Double course;
    protected final Double tax;
    protected final Double fixedCosts;
    protected final String currency;

    public Country(double course, double tax, double fixedCosts, String currency) {
        this.course = course;
        this.tax = tax;
        this.fixedCosts = fixedCosts;
        this.currency = currency;
    }

    public SummaryDTO calculateEarnings(double dailySalary) {
        Double salary = dailySalary * 22;
        double taxValue = salary*tax;
        double earnings = (salary - taxValue - fixedCosts) * course;
        return new SummaryDTO(MathUtils.round(salary, 2), MathUtils.round(taxValue, 2), fixedCosts, MathUtils.round(earnings, 2) , currency);
    }
}