package com.kalkulator.Calculator;

import com.kalkulator.Country.Germany;
import com.kalkulator.Country.Poland;
import com.kalkulator.Country.UnitedKingdom;
import com.kalkulator.Exception.CountryNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public SummaryDTO calculateEarnings(CalculationDTO calculationDTO) throws Exception {
        String countryName = calculationDTO.getCountry();
        double dailySalary = calculationDTO.getDailySalary();
        switch (countryName) {
            case "PL":
                return Poland.getInstance().calculateEarnings(dailySalary);
            case "UK":
                return UnitedKingdom.getInstance().calculateEarnings(dailySalary);
            case "DE":
                return Germany.getInstance().calculateEarnings(dailySalary);
            default:
                throw new CountryNotFoundException("Nie znaleziono takiego kraju");
        }
    }
}