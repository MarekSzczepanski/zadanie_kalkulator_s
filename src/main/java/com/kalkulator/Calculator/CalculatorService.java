package com.kalkulator.Calculator;

import org.springframework.stereotype.Component;

@Component
public interface CalculatorService {
    SummaryDTO calculateEarnings(CalculationDTO calculationDTO) throws Exception;
}