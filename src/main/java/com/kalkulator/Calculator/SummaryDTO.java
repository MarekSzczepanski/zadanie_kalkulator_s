package com.kalkulator.Calculator;

public class SummaryDTO {
    private final double salary;
    private final double taxValue;
    private final double fixedCosts;
    private final double earnings;
    private final String currency;

    public SummaryDTO(double salary, double taxValue, double fixedCosts, double earnings, String currency) {
        this.salary = salary;
        this.taxValue = taxValue;
        this.fixedCosts = fixedCosts;
        this.earnings = earnings;
        this.currency = currency;
    }

    public double getSalary() {
        return salary;
    }

    public double getTaxValue() {
        return taxValue;
    }

    public double getFixedCosts() {
        return fixedCosts;
    }

    public double getEarnings() {
        return earnings;
    }

    public String getCurrency() {
        return currency;
    }
}
