package com.kalkulator.Calculator;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class CalculationDTO {
    @Length(min = 2, max = 3, message = "Niepoprawny skrót kraju")
    private String country;
    @Min(value = 1, message = "Minimalna wartość wynosi 1")
    @Max(value = 10000, message = "Maksymalna wartość wynosi 10 000")
    private double dailySalary;

    public CalculationDTO() {
    }

    public CalculationDTO(String country, double dailySalary) {
        this.country = country;
        this.dailySalary = dailySalary;
    }

    public String getCountry() {
        return country;
    }

    public double getDailySalary() {
        return dailySalary;
    }
}