export class  Summary {
  constructor(public salary:number, public taxValue:number, public fixedCosts:number, public earnings:number, public currency:string){};
}
