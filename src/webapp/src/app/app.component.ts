import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CalculatorService} from "./calculator.service";
import {Summary} from "./summary.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements OnInit{
  signinForm: FormGroup;
  @Input() summary:Summary;

  constructor(private calculatorService: CalculatorService){}

  ngOnInit() {
    this.summary = null;
   this.signinForm = new FormGroup({
     'dailySalary':new FormControl('1', [Validators.min(1), Validators.max(100000)]),
     'country':new FormControl('PL')
   });
  }

  onSubmit() {
    const country = this.signinForm.get('country').value;
    const dailySalary = this.signinForm.get('dailySalary').value;
    this.calculatorService.calculate(country, dailySalary)
      .subscribe(summary => this.summary=summary);
  }

  onReset(){
    this.ngOnInit();
  }

  checkEarnings(){
    return this.summary.earnings < 0 ? 'red' : 'black';
  }
}
