import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Summary} from "./summary.model";


@Injectable()
export class CalculatorService {

  constructor(private httpClient: HttpClient){}

  calculate(country:string, dailySalary:string) {
    const url = 'http://localhost:8080';
    let headers = new HttpHeaders();
    return this.httpClient.post<Summary>(url, {country: country, dailySalary: dailySalary}, {headers: headers});
  }
}
